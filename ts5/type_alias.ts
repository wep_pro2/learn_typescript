type CarYear = number;
type CarType = string;
type CarModel = String;
type Car = {
    year: CarYear,
    tpye: CarType,
    model: CarModel
}

const carYear:CarYear = 2001;
const carType:CarType = "Toyota";
const carModel:CarModel = "Corolla";

const car1:Car = {
    year: 2001,
    tpye: "Nissan",
    model: "XXX"
}
console.log(car1);